﻿Imports CrystalDecisions.Shared
Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Drawing
Imports System.Drawing.Bitmap
Imports System.Reflection


Public Class Reports
	Inherits System.Web.UI.Page

#Region "Declaracion de Variables"

	Public strNombrePagina As String
	
#End Region

#Region "Metodos de Pagina"

	Protected Overrides Sub InitializeCulture()
		Dim lang = Request.QueryString("lang")

		If lang Is Nothing Then
			lang = "en"
		End If

		UICulture = lang
		Culture = lang

		System.Threading.Thread.CurrentThread.CurrentCulture = Globalization.CultureInfo.CreateSpecificCulture(lang)
		System.Threading.Thread.CurrentThread.CurrentUICulture = New CultureInfo(lang)

		MyBase.InitializeCulture()

	End Sub

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		If Not Page.IsPostBack Then
			Dim intTipoVariable = ConfigurationManager.AppSettings("TipoVariable").ToString
			ImprimirReporteAutomatico(getValorVariable("", -1), getValorVariable("", -2), intTipoVariable)
		End If

	End Sub

#End Region

#Region "Utilidades"
	'Metodo utilizado para especificar el tipo de reporte y mostrarlo
	Private Sub Mostrar_Reporte(Tipo As String, Reporte As String)

		Select Case Tipo

			Case "pdf"
				crsOrigenReporte.ReportDocument.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, False, Reporte)
			Case "excel"
				crsOrigenReporte.ReportDocument.ExportToHttpResponse(ExportFormatType.Excel, Response, False, Reporte)
			Case "excelrecord"
				crsOrigenReporte.ReportDocument.ExportToHttpResponse(ExportFormatType.ExcelRecord, Response, False, Reporte)
			Case "crystal"
				crsOrigenReporte.ReportDocument.ExportToHttpResponse(ExportFormatType.CrystalReport, Response, False, Reporte)
			Case "noformat"


		End Select

	End Sub

	Private Sub Desactivar(Optional message As String = "")
		reporteVisor.Visible = False
		crsOrigenReporte.Visible = False
		'pnlError.Visible = True
		'lbError.Text = message
	End Sub

	Public Shared Function Image_to_Byte(Imagen As Image) As Byte()

		Dim boofImagen() As Byte
		Try
			Dim fs As FileStream = New FileStream(Path.GetTempFileName(), FileMode.OpenOrCreate, FileAccess.ReadWrite)
			Imagen.Save(fs, System.Drawing.Imaging.ImageFormat.Png)
			fs.Position = 0
			Dim imlengt As Integer = Convert.ToInt32(fs.Length)
			boofImagen = New Byte(imlengt) {}
			fs.Read(boofImagen, 0, imlengt)
			fs.Close()
			Return boofImagen
		Catch ex As Exception

			boofImagen = New Byte(0) {}
			Return boofImagen
		End Try
	End Function

	'Método utilizado para hacer consultas generales con ADO
	Public Shared Function MetGeneral(Consulta As String) As DataTable
		Dim Tabla As New DataTable()

		Try
			Dim db As New DataClassesDataContext
			Dim Con As New SqlConnection(db.Connection.ConnectionString)
			Dim Adapter As New SqlDataAdapter(Consulta, Con)
			Adapter.Fill(Tabla)
		Catch ex As Exception

		End Try

		Return Tabla
	End Function

#End Region

#Region "Proceso de Filtros Automaticos"

	''' <summary>
	''' Imprime reporte desde un ID
	''' </summary>
	''' <param name="intReporteID"></param>
	''' <param name="strTipoReporte"></param>
	''' <remarks></remarks>
	Private Sub ImprimirReporteAutomatico(intReporteID As Integer, strTipoReporte As String, intVariableABuscar As Integer)

		If IsNumeric(intReporteID) AndAlso intReporteID > 0 Then

			Dim db As New DataClassesDataContext

			Dim objReporte As Reporte = db.Reportes.Where(Function(n) n.ReporteID = intReporteID).FirstOrDefault

			Dim Consulta As String = getConsultaReporte(objReporte, intVariableABuscar)

			'Desventaja de Tener q notificar en el cambio de una Vista. 05 - 06 - 13
			'Dim data = db.ExecuteQuery(Of HUB_view_Buscador)(Consulta + condicion + " order by fechaActividad").CopyToDataTable
			Dim data = MetGeneral(Consulta)
			Dim strRutaArchivoBuscar As String = objReporte.Url + objReporte.Archivo
			Try

				If data.Rows.Count > 0 Then

					strNombrePagina = objReporte.Descripcion

					'Asigna Nombre de Archivo a Mostrar.
					crsOrigenReporte.Report.FileName = strRutaArchivoBuscar

					Try	'Entra en Try por si acaso no existe la foto.

						'Se crea la columna de tipo Byte() para guradar la imagen ya convertida.
						'datos.Columns.Add(New DataColumn("Logo", GetType(Byte())))

						'Se convierte la imagen desde su ubicacion fisica
						'Dim logo() As Byte = Image_to_Byte(New Bitmap(Request.PhysicalApplicationPath + "\" + "Images\Logo.png"))
						'data.Rows(0)("Logo") = logo

					Catch ex As Exception
					End Try

					'Se agrega la data del reporte a mostrar.
					crsOrigenReporte.ReportDocument.SetDataSource(data)

					'Busca la clase de recurso.
					Dim ClaseRecurso As String = objReporte.ResourceFile

					'Bucle de asignacion de valores del recurso.
					If ClaseRecurso IsNot Nothing AndAlso ClaseRecurso.Trim <> "" AndAlso ClaseRecurso.Length > 0 Then
						For Each PField As ParameterField In crsOrigenReporte.ReportDocument.ParameterFields
							Dim valorRecurso As String = CType(GetGlobalResourceObject(ClaseRecurso, PField.Name), String)
							crsOrigenReporte.ReportDocument.SetParameterValue(PField.Name, valorRecurso)
						Next
					End If

					'Mostrar el reporte.
					Me.Mostrar_Reporte(strTipoReporte, objReporte.Nombre)

				Else
					Throw New ArgumentException("ERROR AL CARGAR LOS DATOS")
				End If

			Catch ex As Exception
				Dim strMensajeMostrar As String = ex.Message + "<br>" + strRutaArchivoBuscar
				If ConfigurationManager.AppSettings("VerStackTrace").ToString.Trim = "1" Then
					strMensajeMostrar += "<br>" + ex.StackTrace
				End If
				Desactivar(strMensajeMostrar)
			End Try
		Else
			Desactivar()
		End If

	End Sub

	''' <summary>
	''' Concatena la consulta y los filtros.
	''' </summary>
	''' <param name="objReporte">instacia de st_reportes</param>
	''' <param name="tipo">entero para designar que busca: variable o control.</param>
	''' <returns>retorna la cadena de string que es la consulta mas el filtro.</returns>
	''' <remarks>solo simplifica la concatenacion.</remarks>
	Function getConsultaReporte(objReporte As Reporte, tipo As Integer) As String
		Dim _retVal As String = ""
		Dim _Consulta As String = objReporte.Consulta
		Dim _Condicion = getFiltros(objReporte, tipo)
		_retVal = _Consulta + _Condicion
		Return _retVal
	End Function

	''' <summary>
	''' Devuelve un Where o un And Dependiendo de que tipo sea el filtro.
	''' </summary>
	''' <param name="intCantFiltros">Si es el 1ro o que numero de filtro es.</param>
	''' <returns>Devuelve un String.</returns>
	''' <remarks>Recuerda que el filtro uno sera where.</remarks>
	Function getFiltroAppend(intCantFiltros As Integer) As String
		Dim retVal As String = ""

		If intCantFiltros = 1 Then
			retVal = " WHERE "
		ElseIf intCantFiltros = -1 Then
			retVal = " ORDER BY "
		Else
			retVal = " AND "
		End If

		Return retVal
	End Function

	''' <summary>
	''' Esta Funcion arma una cadena de los filtros necesarios tomando los datos de la base de datos.
	''' </summary>
	''' <param name="obj">Objeto o Instancia de un reporte en la tabla rep_Reportes.</param>
	''' <param name="Es_Variable_O_Control">Tipo de que tipo de variable sale el valor. Variables = 0 , Control = 1.</param>
	''' <returns>Devuelve un String</returns>
	''' <remarks>Favor recordar llenar los valores en la base de datos.</remarks>
	Function getFiltros(obj As Reporte, Optional Es_Variable_O_Control As Integer = 1) As String
		Dim strFiltros As New StringBuilder
		Dim cantFiltros As Integer = 0

		For Each item As Reportes_Filtro In obj.Reportes_Filtros
			Select Case item.Tipo
				Case "int"
					Dim valor As Object = getValorVariable(item.NombreVariable, Es_Variable_O_Control)

					If valor IsNot Nothing AndAlso IsNumeric(valor) Then
						cantFiltros += 1
						strFiltros.Append(getFiltroAppend(cantFiltros))
						strFiltros.Append(" ( " + item.NombreCampo + " = " + valor.ToString() + " ) ")
					End If

				Case "varchar"
					Dim valor As Object = getValorVariable(item.NombreVariable, Es_Variable_O_Control)

					If valor IsNot Nothing AndAlso valor <> "" Then
						cantFiltros += 1
						strFiltros.Append(getFiltroAppend(cantFiltros))
						strFiltros.Append(" ( " + item.NombreCampo + " LIKE '%" + valor + "%' ) ")
					End If

				Case "datetime"
					Dim valorDesde As Object = getValorVariable(item.NombreVariable, Es_Variable_O_Control)
					Dim valorHasta As Object = getValorVariable(item.NombreVariableHasta, Es_Variable_O_Control)
					Dim valFechaDesde As Boolean = False
					Dim valFechaHasta As Boolean = False
					Dim valorPonerFiltro3a As Date = Nothing
					Dim valorPonerFiltro3b As Date = Nothing

					valFechaDesde = DateTime.TryParseExact(valorDesde, "yyyy/MM/dd", CultureInfo.CurrentCulture, DateTimeStyles.None, valorPonerFiltro3a)
					valFechaHasta = DateTime.TryParseExact(valorHasta, "yyyy/MM/dd", CultureInfo.CurrentCulture, DateTimeStyles.None, valorPonerFiltro3b)

					If valFechaDesde AndAlso valFechaHasta Then
						cantFiltros += 1
						strFiltros.Append(getFiltroAppend(cantFiltros))
						strFiltros.Append(" ( " + item.NombreCampo + " BETWEEN '" + valorPonerFiltro3a.ToString("yyyy-MM-dd 00:00:00.000") + "' AND '" + valorPonerFiltro3b.ToString("yyyy-MM-dd 23:59:59.999") + "' ) ")
					End If
			End Select
		Next
		For Each item As Reportes_Filtro In obj.Reportes_Filtros
			Select Case item.Tipo
				Case "orderby"
					strFiltros.Append(getFiltroAppend(-1))
					strFiltros.Append(" " + item.NombreCampo + " ")
			End Select
		Next
		Return strFiltros.ToString
	End Function

	''' <summary>
	''' Obtiene el valor de una variable.
	''' </summary>
	''' <param name="variable">Nombre de una variable</param>
	''' <param name="intTipo">Para especificar el tipo de variable a rescatar o buscar. 
	''' Si es 0 buscar una varible Global,
	''' Si es 1 buscar un nombre de control, 
	''' Si es 2 busca un Query String, 
	''' Si es 3 busca en un Post
	''' </param>
	''' <returns>Objeto</returns>
	''' <remarks>Recuerda que la variable debe de ser declarada publica para ser encontrada.</remarks>
	Function getValorVariable(variable As String, intTipo As Integer, Optional intTipoNegativo As Integer = 2) As Object

		Dim retVal As Object = Nothing

		Select Case intTipo
			Case -2	' Para que me devuelva el 2do valor del arreglo.
				Dim intVal As Integer = 0

				Select Case intTipoNegativo
					Case 2 ' Proviene de Query String
						For Each req In Request.QueryString()
							intVal += 1
							Dim _NombreControl As String = req.ToString
							Dim _ValorControl As String = Request.QueryString(_NombreControl).ToString
							If intVal = 2 Then
								If (_ValorControl = "excel" OrElse _
									_ValorControl = "excelrecord" OrElse _
									_ValorControl = "noformat" OrElse _
									_ValorControl = "crystal") Then
									retVal = _ValorControl
								Else
									retVal = "pdf"
								End If
								Exit For
							Else
								retVal = "pdf"
							End If
						Next
					Case 3 ' Proviene de un Post
						For Each req In Request.Form()
							intVal += 1
							Dim _NombreControl As String = req.ToString
							Dim _ValorControl As String = Request.Form(_NombreControl).ToString
							If intVal = 2 Then
								If (_ValorControl = "excel" OrElse _
									_ValorControl = "excelrecord" OrElse _
									_ValorControl = "noformat" OrElse _
									_ValorControl = "crystal") Then
									retVal = _ValorControl
								Else
									retVal = "pdf"
								End If
								Exit For
							End If
						Next

				End Select

			Case -1	' Para que me de el primer valor del arreglo.

				Select Case intTipoNegativo
					Case 2 ' QUERY STRING
						For Each req In Request.QueryString()
							Dim _NombreControl As String = req.ToString
							Dim _ValorControl As Object = Request.QueryString(_NombreControl)
							retVal = _ValorControl
							Exit For
						Next
					Case 3 'POST
						For Each req In Request.Form()
							Dim _NombreControl As String = req.ToString
							Dim _ValorControl As Object = Request.Form(_NombreControl)
							retVal = _ValorControl
							Exit For
						Next
				End Select

			Case 0

				Dim fields As FieldInfo() = Me.GetType().GetFields()

				For Each fld As FieldInfo In fields
					Dim name As String = fld.Name
					Dim value = fld.GetValue(Me)
					Dim typ As Type = fld.FieldType
					If name = variable Then
						retVal = value
						Exit For
					End If
				Next

			Case 1

				Dim c As Object = FindControl(variable)
				If TypeOf c Is TextBox Then
					retVal = DirectCast(c, TextBox).Text
				ElseIf TypeOf c Is CheckBox Then
					retVal = DirectCast(c, CheckBox).Checked
				ElseIf TypeOf c Is DropDownList Then
					retVal = DirectCast(c, DropDownList).SelectedValue
				End If

			Case 2 ' Query String

				For Each req In Request.QueryString()
					Dim arreglo = req.ToString.Split("$")
					Dim NumeroMaximo = arreglo.Count
					If arreglo.GetValue(NumeroMaximo - 1) = variable Then
						Dim _NombreControl As String = req.ToString
						Dim _ValorControl As Object = Request.QueryString(_NombreControl)
						retVal = _ValorControl
						Exit For
					End If
				Next

			Case 3 ' Post String

				For Each req In Request.Form()
					Dim arreglo = req.ToString.Split("$")
					Dim NumeroMaximo = arreglo.Count
					If arreglo.GetValue(NumeroMaximo - 1) = variable Then
						Dim _NombreControl As String = req.ToString
						Dim _ValorControl As Object = Request.Form(_NombreControl)
						retVal = _ValorControl
						Exit For
					End If
				Next

		End Select

		Return retVal

	End Function

#End Region

End Class