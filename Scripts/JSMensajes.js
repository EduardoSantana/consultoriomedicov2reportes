﻿var myMessages = ['info', 'warning', 'error', 'success']; // define the messages types	

function MostrarMensaje(type, Mensaje) {
    $('#' + type).css({ display: "block", "z-index": "700000" });
    $('#' + type).html('<h3 style="background-color:transparent;text-transform:none; ">' + Mensaje + '</h3>');
    $('#' + type).animate({ top: "0", opacity: "1" }, 800);
}

function ocultar(type) {

    var altura = $('#' + type).outerHeight();
    $('#' + type).animate({ top: -altura, opacity: "0" }, 2000, function () {
        $('#' + type).css({ display: "none" });
    });

}

$(document).ready(function () {

    $('.message').click(function () {
        $(this).animate({ top: -$(this).outerHeight(), opacity: "0" }, 500, function () {
            $(this).css({ display: "none" });
        });
    });

});

function ComprobarIMEI(idtext, lenght) {
    var imei1 = document.getElementById('ContentPlaceHolder1_Registro_txtIMEI').value;
    var imei2 = document.getElementById(idtext).value;
    var longitud = imei2.length;
    if (longitud != lenght) {
        MostrarMensaje('Error', 'El Numero de IMEI debe Tener ' + lenght + ' digitos. !!');
        return false;
    }

    if (imei1.trim() != '') {

        if (imei1.trim() != imei2.trim()) {
            MostrarMensaje('Advertencia', 'Diferencia entre IMEI Operador e IMEI Cliente !!');
        }
        else {
            ocultar('Advertencia');
        }
    }

}

function verificar_destino(bt) {
    if ($("#dd_Destino").val() == "0") {
        document.getElementById('dd_Destino').focus();
    }
    else {
        bt.click();
    }
}

function setFocus() {
    try {
        obFoco = document.getElementById('txtFoco').value;
        document.getElementById('ContentPlaceHolder1_' + obFoco).focus();
    } catch (e) {
    }
}

///SObrecarga de la funcion MostrarModalTitulo
function MostrarModal(mensaje, bt) {

    return MostrarModalTitulo('CONFIRMACION', mensaje, bt, null);
}

///Esta funcion Sirve para mostrar un formulario modal en forma de pregunta para ver
///si se hara o no el postback
function MostrarModalTitulo(titulo, mensaje, bt, func, informacion) {

    ShowModal("dvModalHijo", true, "", "", 200000);
    $("#mdMensaje").text(mensaje);
    $("#mdTitulo").text(titulo);
    $("#mdAceptar").focus();

    //Se extrae la funcion del boton y se gurda en la variable vieja_func
    var vieja_func;
    if (bt != null) {
        vieja_func = bt.onclick;
        bt.onclick = null;
        $("#" + bt.id).unbind("click");
        $("#mdCancelar").css("display", "inline");
    }
    else {
        if(func == null)
            $("#mdCancelar").css("display", "none");

        $("#mdAceptar").unbind("click");
    }

    //Se llama al boton aceptar del modal y se le asigna la funcion del boton en el evento click 
    $("#mdAceptar").bind("click", function () {
        HideModal("dvModalHijo");

        if ($.type(func) === "function") {
            func();
            $("#" + bt.id).bind("click", vieja_func);
        } else if (bt != null) {
            bt.onclick = null;
            $("#" + bt.id).unbind("click");
            bt.click();
            $(bt).attr("disabled", "disabled");
        }
    });

    //Se le asigna al boton cancelar del modal una function para cerrar el modal y colocar el boton como estaba anteriormente
    $("#mdCancelar").bind("click", function () {
        HideModal("dvModalHijo");
        bt.onclick = null;
        $("#" + bt.id).bind("click", vieja_func);

        document.getElementById("mdAceptar").onclick = null;
    });

    //Se retorna false para evitar que el boton haga el postback al ser pulsado
    return false;
}

function poner(even, tr) {
    var dv = tr.parentElement;
    $("#" + dv.id).bind("mousemove", function (event) {
        var tp = event.pageY - even.layerY;
        var hg = event.pageX - even.layerX;
        dv.style.top = tp + "px";
        dv.style.left = hg + "px";

    });
}

function quitar(tr) {
    var dv = tr.parentElement;
    $("#" + dv.id).unbind("mousemove");
}

function verficar_Imei() {

    var imei1 = $("#spIMEI").text().replace("IMEI:", "");
    var imei2 = $("#txtIMEI").val();

    if (imei1.trim() == imei2.trim()) {

        $("#td_IMEI").css({ border: "1px solid black", "background-color": "green", "background-image": "url(../../images/barraCorrecto.png)" });
        $("#td_IMEI").html("Número de IMEI Correcto !!");
        $("#Imei_aceptar").removeAttr("disabled");

    }
    else {
        $("#td_IMEI").css({ border: "1px solid black", "background-color": "red", "background-image": "url(../../images/barraError.png)" });
        $("#td_IMEI").html("Este Número de IMEI es Incorrecto !!");
        $("#Imei_aceptar").attr("disabled", "disabled");

    }
}

function MostrarModalImei(titulo, mensaje, imei, bt, func) {

    $("#td_IMEI").css({ border: "none", "background-color": "#FFF", "background-image": "none" });
    $("#td_IMEI").html("");
    $("#Imei_aceptar").attr("disabled", "disabled");
    $("#txtIMEI").val("");
    $("#spIMEI").text(imei);

    ShowModal("dvImeiHijo", true, "", "Imei_cancelar", 50000);

    $("#Imei_aceptar").attr("disabled", "disabled");
    $("#mdMensaje").text(mensaje);
    $("#mdTitulo").text(titulo);
    $("#Imei_aceptar").bind("click", function () {

        MostrarModalTitulo(titulo, mensaje, bt, func);
    });

    return false;
}

///Funcion general para ocultar un div haciendo click en un h3 que se encuentre dentre de este
///Nota:El evento click debe tenerlo el h3 y los elementos acultar deben estar contenidos en un segundo div
///hijo del primero.
function ContraerDiv(h3) {
    var dv = h3.parentElement;
    var div = $("#" + dv.id).children("div").first();
    div.toggle(500, function () {

        if (div.css("display") == "none") {
            $("#" + dv.id + " i").animate({ opacity: "1" }, 500);

        } else {
            $("#" + dv.id + " i").animate({ opacity: "0" }, 500);
        }
    });
}

function verificarExpr(txt, exp, coment) {

    if (txt.value.trim() != "" && !txt.value.match(exp)) {
        MostrarMensaje('Error', coment);
        return false;

    }
    else
        ocultar('Error');
}

function ToolTip(obj_nombre, info) {

    var obj = document.getElementById(obj_nombre);
    var divs = '<div id="dvtxt_Pikito" class="dvtxt_Pikito">' +
        '<div id="dvPikito" class="cldvPikito">' +
                  '</div>' +
       '<div id="dvTexto" class="cldvTexto">' +
       '<span id ="spMensaje"></span>' +
       '</div>' +
     '</div>';
    if (obj.style.position == "")
        obj.style.position = "relative";

    obj.setAttribute("autocomplete", "off");

    $(obj).mouseover(function () {
        $(divs).appendTo(obj.parentElement);

        $("#dvTexto").children("span").first().html(info);
        var dvtxt_Pikito = document.getElementById("dvtxt_Pikito");
        var dvPikito = document.getElementById("dvPikito");
        if (info.length > 50)
            $(dvtxt_Pikito).css({ width: "350px" });


        $(dvtxt_Pikito).click(function () {
            $(dvtxt_Pikito).animate({ opacity: "0" }, 300, function () {
                $(dvtxt_Pikito).remove();
            });
        });

        var left = obj.offsetLeft;
        if (left >= 9)
            left -= 9;

        $(dvtxt_Pikito).css({ top: obj.offsetTop + obj.offsetHeight + 20, left: left, display: "block" });
        var left2 = String((obj.offsetWidth / 2) - 3);
        dvPikito.style.left = String(left2) + "px";

        $(dvPikito).addClass("arriba");
        $(dvPikito).css({ top: "-15px" });

        var posicion = obj.getBoundingClientRect();

        $(dvPikito).removeClass();
        $(dvPikito).addClass("cldvPikito");

        if ((posicion.bottom + 20 + dvtxt_Pikito.offsetHeight) > window.innerHeight && (dvtxt_Pikito.offsetWidth + 20) < posicion.left) {
            var i = posicion.left - (dvtxt_Pikito.offsetWidth + obj.offsetWidth);
            $(dvtxt_Pikito).css({ top: obj.offsetTop, left: i });
            var top = (dvtxt_Pikito.offsetHeight - dvPikito.offsetHeight) / 2;

            $(dvPikito).addClass("derecha");
            $(dvPikito).css({ top: top, left: dvtxt_Pikito.offsetWidth - 6 });
        }
        else if ((posicion.bottom + 20 + dvtxt_Pikito.offsetHeight) > window.innerHeight && (dvtxt_Pikito.offsetWidth + 20) < posicion.right) {
            var i = obj.offsetLeft + obj.offsetWidth + 20;
            $(dvtxt_Pikito).css({ top: obj.offsetTop, left: i });
            var top = (dvtxt_Pikito.offsetHeight - dvPikito.offsetHeight) / 2;
            $(dvPikito).addClass("izquierda");
            $(dvPikito).css({ top: top, left: "-19px" });

        }

    });

    $(obj).mouseout(function () {
        $("#dvtxt_Pikito").remove();
    });
}

function ToolTipInstant(obj_nombre, info) {

    if (info.trim() == "")
        return;

    var obj = document.getElementById(obj_nombre);
    var divs = '<div id="dvtxt_Pikito" class="dvtxt_Pikito">' +
        '<i id="dvPikito" class="cldvPikito">' +
                  '</i>' +
       '<div id="dvTexto" class="cldvTexto">' +
       '<span id ="spMensaje"></span>' +
       '</div>' +
     '</div>';
    if (obj.style.position == "")
        obj.style.position = "relative";

    obj.setAttribute("autocomplete", "off");

    $(divs).appendTo(obj.parentElement);
    $("#dvTexto").children("span").first().html(info);
    var dvtxt_Pikito = document.getElementById("dvtxt_Pikito");
    var dvPikito = document.getElementById("dvPikito");
    if (info.length > 50)
        $(dvtxt_Pikito).css({ width: "350px" });


    $(dvtxt_Pikito).click(function () {
        $(dvtxt_Pikito).animate({ opacity: "0" }, 300, function () {
            $(dvtxt_Pikito).remove();
        });
    });

    var left = obj.offsetLeft;
    if (left >= 9)
        left -= 9;

    $(dvtxt_Pikito).css({ top: obj.offsetTop + obj.offsetHeight + 20, left: left, display: "block" });
    var left2 = String((obj.offsetWidth / 2) - 3);
    dvPikito.style.left = String(left2) + "px";

    $(dvPikito).addClass("arriba");
    $(dvPikito).css({ top: "-15px" });

    var posicion = obj.getBoundingClientRect();

    $(dvPikito).removeClass();
    $(dvPikito).addClass("cldvPikito");

    if ((posicion.bottom + 20 + dvtxt_Pikito.offsetHeight) > window.innerHeight && (dvtxt_Pikito.offsetWidth + 20) < posicion.left) {
        var i = posicion.left - (dvtxt_Pikito.offsetWidth + obj.offsetWidth - 45);
        $(dvtxt_Pikito).css({ top: obj.offsetTop, left: i });
        var top = (dvtxt_Pikito.offsetHeight - dvPikito.offsetHeight) / 2;

        $(dvPikito).addClass("derecha");
        $(dvPikito).css({ top: top, left: dvtxt_Pikito.offsetWidth - 6 });
    }
    else if ((posicion.bottom + 20 + dvtxt_Pikito.offsetHeight) > window.innerHeight && (dvtxt_Pikito.offsetWidth + 20) < posicion.right) {
        var i = obj.offsetLeft + obj.offsetWidth + 20;
        $(dvtxt_Pikito).css({ top: obj.offsetTop, left: i });
        var top = (dvtxt_Pikito.offsetHeight - dvPikito.offsetHeight) / 2;
        $(dvPikito).addClass("izquierda");
        $(dvPikito).css({ top: top, left: "-19px" });


    }

    setTimeout('$( "#dvtxt_Pikito" ).animate({ opacity: "0" },300, function (){$("#dvtxt_Pikito").remove();});', 10000);


}

function getDimensions(oElement) {
    var x, y, w, h;
    x = y = w = h = 0;
    if (document.getBoxObjectFor) { // Mozilla
        var oBox = document.getBoxObjectFor(oElement);
        x = oBox.x - 1;
        w = oBox.width;
        y = oBox.y - 1;
        h = oBox.height;
    }
    else if (oElement.getBoundingClientRect) { // IE
        var oRect = oElement.getBoundingClientRect();
        x = oRect.left - 2;
        w = oElement.clientWidth;
        y = oRect.top - 2;
        h = oElement.clientHeight;
    }
    if (oElement.style.position == "absolute")
        return { left: oElement.offsetLeft, top: oElement.offsetTop, width: oElement.offsetWidth, height: oElement.offsetHeight };
    else
        return { left: x, top: y, width: w, height: h };
}

function ShowModal(id_dv, modal, clase, button, zindex) {
    //Se Relaciona el tamaño del modal con el tamaño de la pantalla para centralizarlo
    var dv = document.getElementById(id_dv);
    var alto = innerHeight - Number($(dv).css("height").replace("px", ""));
    var ancho = document.body.clientWidth - Number($(dv).css("width").replace("px", ""));
    var index = 600000;

    if (zindex && zindex > 0)
        index = zindex;

    alto = alto / 2;
    ancho = ancho / 2;

    alto += document.body.scrollTop;
    $(dv).css({ "z-index": index + 1, display: "block", left: ancho, top: alto, position: "absolute" });

    if (modal = ! null && modal == true) {
        $("#dvModalTemp-" + id_dv).remove();
        var dvModal = $("<div id='dvModalTemp-" + id_dv + "' class='" + clase + "'>  </div>").appendTo("body");
        $(dvModal).css({ top: "0", left: "0", position: "absolute", "z-index": index, display: "block", width: document.body.clientWidth, height: document.body.offsetHeight });

        if (clase == null || clase == '') {
            $(dvModal).css({ "background-color": "blue", opacity: "0.2" });
        }
    }

    window.onresize = null;
    window.onresize = function () {
        ShowModal(id_dv, modal, clase, button, zindex);
    };

    window.onscroll = function () {
        ShowModal(id_dv, modal, clase, button, zindex);
    };
    $("#" + button).css("cursor", "pointer");
    $("#" + button).click(function () {
        HideModal(id_dv, "dvModalTemp-" + id_dv);
    });
}

function HideModal(id_dv, modal) {
    window.onresize = null;
    window.onscroll = null;

    $("#" + id_dv).css({ top: "-500px", left: "-500px", display: "none" });

    if (modal != null)
        $("#" + modal).css({ top: "-500px", left: "-500px", display: "none" });
    else
        $("#dvModalTemp-" + id_dv).remove();

}