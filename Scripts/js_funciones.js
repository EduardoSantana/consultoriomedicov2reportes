﻿
//Funcion para obtener un valor numérico desde un string y en caso de no serlo devolverá cero.
function obtenerDecimal(o) {
    try {
        if (o.length == 0) {
            return 0;
        } else if (parseFloat(o)) {
            return o;
        }
    } catch (e) {
        return 0;
    }
    return 0;
}


//Funcion usada para obtener el texto de una caja de texto
function Valor(o) {
    if (Existe(o)) {
        return document.getElementById(o).value.replace(",", "");
    }
    alert('El objeto ' + o + ', no existe en el la pagina. mande este mensaje al departamento de informática');
    return '';
}


//Funcion usada para determinar si existe un determinado objeto
function Existe(o) {
    for (i = 0; i < formDOA.elements.length; i++) {
        // alert(formDOA.elements[i].id);
        if (formDOA.elements[i].id == o) {
            return true
        }
    }
}

//Funcion usada para determinar si existe un determinado objeto
function ExisteObjeto(o) {
    for (i = 0; i < FormImpresionReporte.elements.length; i++) {
        // alert(formDOA.elements[i].id);
        if (FormImpresionReporte.elements[i].id == o) {
            return true
        }
    }
}


//Función para pero que solo se introduzca un número en una caja de texto
function ValidNum(e) {
    //alert(window.event);
    var tecla = document.all ? tecla = e.keyCode : tecla = e.which;
    //$("#cap").val(tecla);
    //Para que permita mostrar los caracteres de determinadas teclas
    return ((tecla > 47 && tecla < 58) // Numeros del teclado querty
        || (tecla > 95 && tecla < 106) // Numeros del teclado numérico
        || tecla == 46 // 
        || tecla == 110 // El Punto teclado numérico
        || tecla == 190 // El Punto teclado querty
        || tecla == 37 // Techa Izquierda
        || tecla == 39 // Tecla Derecha
        || tecla == 8  // Tecla Tab
        || tecla == 9 //
    ) &&
        !(window.event && window.event.keyCode == 13);
}

function noenter() {
    return !(window.event && window.event.keyCode == 13);

}

//Función usada para devolver un número en formato de decimales.
function currency(value, decimals, separators) {
    decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
    separators = separators || ['.', "'", ','];
    var number = (parseFloat(value) || 0).toFixed(decimals);
    if (number.length <= (4 + decimals))
        return number.replace('.', separators[separators.length - 1]);
    var parts = number.split(/[-.]/);
    value = parts[parts.length > 1 ? parts.length - 2 : 0];
    var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
separators[separators.length - 1] + parts[parts.length - 1] : '');
    var start = value.length - 6;
    var idx = 0;
    while (start > -3) {
        result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
    + separators[idx] + result;
        idx = (++idx) % 2;
        start -= 3;
    }
    return (parts.length == 3 ? '-' : '') + result;
}