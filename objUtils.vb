﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Runtime.CompilerServices
Namespace ClaseAImportar

	Public Class objUtils
		Public Sub objUtils()

		End Sub

        Public Function CopyToDataTable(Of T)(ByVal source As IEnumerable(Of T)) As DataTable

            Return New ObjectShredder(Of T)().Shred(source, Nothing, Nothing)
        End Function

		Public Function CopyToDataTable(Of T)(ByVal source As IEnumerable(Of T), ByVal table As DataTable, ByVal options As LoadOption?) As DataTable
			Return New ObjectShredder(Of T)().Shred(source, table, options)
		End Function
	End Class

End Namespace
