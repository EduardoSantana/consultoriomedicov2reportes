﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Reports.aspx.vb" Inherits="ConsultorioMedicoV2Reportes.Reports" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>
		<% Response.Write(strNombrePagina)%>
	</title>
	
	<script type="text/javascript" src="../../Scripts/jquery-2.0.1.min.js"></script>
	<script type="text/javascript" src="../Scripts/jquery-2.0.1.min.js"></script>
	<script src="../Scripts/Ventana.js" type="text/javascript"></script> 
	<script src="../Scripts/JSMensajes.js" type="text/javascript"></script>   
	<script src="../Scripts/js_funciones.js" type="text/javascript"></script>


	 <style type="text/css">
		.ErrorInforme
		{
			margin: 0px 0px 0px 0px;
			background-color: #dfe2f8;
			font-size: 0px;
			color: navy;
			text-align: center;
			vertical-align: middle;
			font-weight: bold;
			display: block;
			opacity: 0;
			filter: alpha(opacity=0);
			height: 100%;
			position:absolute;
			width:100%;
		}

		.auto-style1
		{
			width: 100%;
		}

		.auto-style2
		{
			width: 267px;
		}

		.auto-style3
		{
			width: 150px;
			height: 177px;
		}
	</style>

	<script lang="javasacript" type="text/javascript">
		function MostrarError() {
			if (document.getElementById('pnlError')) {
				document.getElementById('pnlError').style.opacity = 0.1;
				document.getElementById('pnlError').style.height = 0;
				$('#pnlError').animate({ opacity: "1", height: "400px", top: "-300" }, 1000, function () {
					$(this).animate({ top: "0px" }, 100, function () {
						$(this).animate({ height: "700px", fontSize: "25px" }, 500)
					});
				});
			}
		}
	</script>

	
</head>
<body onload="MostrarError();">
	<form id="form1" runat="server">
		<div>
			<asp:Panel ID="pnlError" runat="server" Height="800px" ClientIDMode="Static" CssClass="ErrorInforme" Visible="false" style="opacity:0.1;left:0; filter:alpha(opacity=50)">
				<br />
				<table class="auto-style1">
					<tr>
						<td class="auto-style2">
							<img alt="" class="auto-style3" src="../images/imagen_de_fondo.gif" />
						</td>
						<td style="text-align: left;">
							<asp:Label ID="Label1Estatico" runat="server" Text="Se ha presentado un error."></asp:Label>
							<br />
							<br />
							<asp:Label ID="lbError" runat="server" Text="No se pudo mostrar el informe"></asp:Label>
						</td>
					</tr>
				</table>
			</asp:Panel>


			<CR:CrystalReportViewer  runat="server" AutoDataBind="True" ID="reporteVisor" 
				GroupTreeImagesFolderUrl="" Height="1200px" 
				ReportSourceID="crsOrigenReporte" ToolbarImagesFolderUrl="" 
				ToolPanelWidth="200px" Width="1200px" ToolPanelView="None" 
				EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False">
			</CR:CrystalReportViewer>
			<CR:CrystalReportSource ID="crsOrigenReporte" runat="server">
				<Report FileName="Reportes\ReporteFactura.rpt">
				</Report>
			</CR:CrystalReportSource>

	</div>
	</form>
    <div>funcionaa</div>
</body>
</html>
